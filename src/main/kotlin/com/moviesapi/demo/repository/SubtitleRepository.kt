package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.Subtitle
import org.springframework.data.repository.CrudRepository

interface SubtitleRepository:CrudRepository<Subtitle,Long> {
}