package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.Cinema
import org.springframework.data.repository.CrudRepository

interface CinemaRepository : CrudRepository<Cinema, Long>
