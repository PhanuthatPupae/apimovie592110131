package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Ticket
import com.moviesapi.demo.repository.TicketRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class TicketDaoDBImpl : TicketDao{
    override fun saveList(mapTicket: MutableIterable<Ticket>): MutableIterable<Ticket> {
       return  ticketRepository.saveAll(mapTicket)
    }

    override fun findById(id: Long): Ticket? {
        return ticketRepository.findById(id).orElse(null)
    }

    override fun save(mapTicket: Ticket): Ticket {
        return ticketRepository.save(mapTicket)
    }

    override fun getTicketByCustomer(email: String, firstName: String): List<Ticket> {
        return ticketRepository.findByCustomer_EmailContainingIgnoreCaseOrCustomer_FirstName(email,firstName)
    }

    override fun getTicketByMovie(name: String):List<Ticket> {
        return ticketRepository.findByCycleTime_Movie_NameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var ticketRepository: TicketRepository
    override fun getAllTicket(): List<Ticket> {
        return ticketRepository.findAll().filterIsInstance(Ticket::class.java)
    }
}