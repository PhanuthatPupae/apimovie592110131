package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Movie
import com.moviesapi.demo.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class MovieDaoDBImpl:MovieDao {
    @Autowired
    lateinit var movieRepository: MovieRepository
    override fun getMovies(): List<Movie> {
      return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }
}