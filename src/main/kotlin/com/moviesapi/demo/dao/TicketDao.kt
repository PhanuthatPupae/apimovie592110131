package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Ticket

interface TicketDao {
    fun getAllTicket(): List<Ticket>
    fun getTicketByMovie(name: String): List<Ticket>
    fun getTicketByCustomer(email: String, firstName: String): List<Ticket>
    fun save(mapTicket: Ticket): Ticket
    fun saveList(mapTicket: MutableIterable<Ticket>): MutableIterable<Ticket>
    fun findById(id: Long): Ticket?
}