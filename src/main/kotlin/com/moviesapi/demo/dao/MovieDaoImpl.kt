package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Movie
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
@Profile("mem")
@Repository
class MovieDaoImpl  : MovieDao{
    override fun getMovies(): List<Movie> {

        return mutableListOf(Movie("007 - 00 James Bond Story, The","test","James Bond","60 min","test"))
    }


}