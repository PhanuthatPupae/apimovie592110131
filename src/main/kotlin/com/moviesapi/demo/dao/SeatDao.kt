package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Seat

interface SeatDao {
    fun getSeat ():List<Seat>
}