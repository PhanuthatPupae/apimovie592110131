package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Customer
import com.moviesapi.demo.repository.CustomerRepository
import com.moviesapi.demo.security.entity.AuthorityName
import com.moviesapi.demo.security.entity.JwtUser
import com.moviesapi.demo.security.repository.AuthorityRepository
import com.moviesapi.demo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository
import javax.transaction.Transactional


@Profile("db")
@Repository
class CustomerDaoDBImpl : CustomerDao {
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    override fun save(mapCustomer: Customer): Customer {
        val authority = authorityRepository.findByName(AuthorityName.ROLE_CUSTOMER)
        val encoder = BCryptPasswordEncoder()
        val jwt = userRepository.save(JwtUser(mapCustomer.email, mapCustomer.firstName, mapCustomer.lastName, encoder.encode(mapCustomer.password), mapCustomer.email, true))
        val customer = customerRepository.save(mapCustomer)
        jwt.user = customer
        customer.jwtUser = jwt
        jwt.authorities.add(authority)
        return customerRepository.save(customer)
    }

    @Transactional
    override fun findById(id: Long): Customer {
        return customerRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomer(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }
}