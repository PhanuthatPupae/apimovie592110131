package com.moviesapi.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


@Entity
data class Sound (var name:String?=null){
    @Id
    @GeneratedValue
    var id:Long? =null
}