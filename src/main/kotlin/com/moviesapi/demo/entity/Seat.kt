package com.moviesapi.demo.entity

import javax.persistence.*


@Entity
data class Seat(var seatType : SeatType = SeatType.DELUXE,
                @Column(name="seatRow")
                var row :Int?=null,
                @Column(name="seatCol")
                var column:Int?=null) {

    @Id
    @GeneratedValue
    var id:Long? =null
}