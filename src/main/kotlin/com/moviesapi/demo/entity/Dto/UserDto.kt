package com.moviesapi.demo.entity.Dto

class UserDto(var email: String? = null,

              var password: String? = null,
              var authorites: List<AuthorityDto> = mutableListOf()
)