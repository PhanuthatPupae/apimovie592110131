package com.moviesapi.demo.entity.Dto

data class SoundDto(var name: String? = null,
                    var id: Long? = null
)