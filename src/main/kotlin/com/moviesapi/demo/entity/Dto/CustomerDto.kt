package com.moviesapi.demo.entity.Dto

class CustomerDto(var id:Long?=null,
                  var firstName: String? = null,
                  var lastName:String?=null,
                  var email: String?=null,
                  var password: String?=null,
                  var image: String? =null
                 ) {
}