package com.moviesapi.demo.entity

import javax.persistence.*


@Entity
data class Cinema(var name: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToMany
    var Seat = mutableListOf<Seat>()
}