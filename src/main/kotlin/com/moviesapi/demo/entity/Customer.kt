package com.moviesapi.demo.entity

import com.moviesapi.demo.security.entity.JwtUser
import org.springframework.web.multipart.MultipartFile
import javax.persistence.*


@Entity
data class Customer(override var firstName: String? = null,
                    override var lastName: String? = null,
                    override var userStatus: UserStatus? = UserStatus.ACTIVE,
                    override var email: String? = null,
                    override var password: String? = null,
                    override var image: String? =null




) : User( firstName,lastName, userStatus, email, password, image) {




}



