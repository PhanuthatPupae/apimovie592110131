package com.moviesapi.demo.entity

enum class UserStatus {
    ACTIVE,PENDING,NOTACTIVE,DELETED
}