package com.moviesapi.demo.entity

import javax.persistence.*


@Entity
class Ticket(var seatName: String? = null, var seatStatus: Boolean = false,
             @Column(name = "seatRow")
             var row: Int? = null,
             @Column(name = "seatCol")
             var column: Int? = null,
             var price:Int? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var cycleTime: CycleTime? = null

    @ManyToOne
    var customer: Customer?=null
}