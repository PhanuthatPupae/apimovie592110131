package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.entity.Dto.TicketDto
import com.moviesapi.demo.entity.Ticket

import com.moviesapi.demo.service.TicketService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
class TicketController {
    @Autowired
    lateinit var ticketService: TicketService

    @GetMapping("/ticket")
    fun getAllTicket(): ResponseEntity<Any> {
        val allTicket = ticketService.getAllTicket()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapTicketDto(allTicket))

    }

    @GetMapping("/ticket/{movie}")
    fun getTicketByMovieNAme(@PathVariable("movie") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapTicketDto(ticketService.getTicketByMovie(name))
        return ResponseEntity.ok(output)
    }
//
//    @GetMapping("/ticket/page/movie")
//    fun getTicketWithPage(@RequestParam("name")name:String,@RequestParam("page")page:Int,@RequestParam("pageSize")pageSize:Int):ResponseEntity<Any>{
//        val output = MapperUtil.INSTANCE.mapTicketDto(ticketService.getTicketByMovie(name))
//        val withPage = ticketService.getTicketWithPage(output,page,pageSize)
//        return ResponseEntity.ok(output)
//    }

    @GetMapping("ticket/customer")
    fun getTicketByCustomer(@RequestParam("email") email: String,
                            @RequestParam("firstName", required = false
                            ) firstName: String?): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapTicketDto(ticketService.getTicketByCustomer(email, email))
        return ResponseEntity.ok(output)
    }
    @PostMapping("/ticket/customer/{id}")
    fun addTicketByCustormer(@RequestBody ticketDto: TicketDto,@PathVariable id:Long):ResponseEntity<Any>{
        val output = ticketService.save(id,MapperUtil.INSTANCE.mapTicket(ticketDto))
        val outputDto = MapperUtil.INSTANCE.mapTicketDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/ticket/cycletime/{id}")
    fun addTicketByCycleTime(@RequestBody ticketDto: TicketDto,@PathVariable id:Long):ResponseEntity<Any>{
        val output = ticketService.saveTicketByCycleTime(id,MapperUtil.INSTANCE.mapTicket(ticketDto))
        val outputDto = MapperUtil.INSTANCE.mapTicketDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/ticketlist/list")
    fun addTicketByCycleTimeList(@RequestBody ticketDto: MutableIterable<TicketDto>):ResponseEntity<Any>{
        val output = ticketService.saveTicketByList(MapperUtil.INSTANCE.mapTicketList(ticketDto))
        val outputDto = MapperUtil.INSTANCE.mapTicketLists(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PutMapping("/ticket/{id}")
    fun update(@PathVariable (value = "id")id:Long?,@RequestBody ticketDto: TicketDto):ResponseEntity<Any> {
        ticketDto.id = id
        val output = ticketService.saveAll(MapperUtil.INSTANCE.mapTicket(ticketDto))
        val outputDto = MapperUtil.INSTANCE.mapTicketDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()


    }
    @PutMapping("/ticket/{id}/customer/{idCustomer}")
    fun updateTicketByCustormer(@RequestBody ticketDto: TicketDto,@PathVariable id:Long?,@PathVariable  idCustomer:Long):ResponseEntity<Any>{
        ticketDto.id = id
        val output = ticketService.updateBycustomer(idCustomer,MapperUtil.INSTANCE.mapTicket(ticketDto))
        val outputDto = MapperUtil.INSTANCE.mapTicketDto(output)
        outputDto?.let { return  ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

}