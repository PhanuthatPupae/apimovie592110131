package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.entity.Movie
import com.moviesapi.demo.service.MovieService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class MovieController {
    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/movie")
    fun getMovies(): ResponseEntity<Any> {
        val movies = movieService.getMovies()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapMovieDto(movies))

    }
}