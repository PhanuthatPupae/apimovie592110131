package com.moviesapi.demo.controller

import com.moviesapi.demo.service.AmazonClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile


@RestController
class BucketController {
    @Autowired
    lateinit var amazonClient:AmazonClient

    @PostMapping("/uploadFile")
    fun uploadFile(@RequestPart("file" )file:MultipartFile):ResponseEntity<Any>{
        return ResponseEntity.ok(this.amazonClient.uploadFile(file))


    }
    @DeleteMapping("/deleteFile")
    fun deleteFile(@RequestPart(value ="url")fileUrl:String):ResponseEntity<Any>{
        return ResponseEntity.ok(this.amazonClient.deleteFileFromS3Bucket(fileUrl))
    }
}