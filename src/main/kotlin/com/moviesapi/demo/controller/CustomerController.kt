package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.entity.Customer
import com.moviesapi.demo.entity.Dto.CustomerDto
import com.moviesapi.demo.repository.CustomerRepository
import com.moviesapi.demo.security.entity.AuthorityName
import com.moviesapi.demo.security.entity.JwtUser
import com.moviesapi.demo.security.repository.AuthorityRepository
import com.moviesapi.demo.security.repository.UserRepository

import com.moviesapi.demo.service.CustomerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService
    @Autowired
    lateinit var userRepository: UserRepository


    @GetMapping("/customer")
    fun getCustomer(): ResponseEntity<Any> {
        var customer = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customer))
    }

    @PostMapping("/customer/reg")
    fun addCustomer(@RequestBody customerDto: CustomerDto): ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomer(customerDto))

        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
//        val authority = authorityRepository.findByName(AuthorityName.ROLE_CUSTOMER)
//        val encoder = BCryptPasswordEncoder()
//        val jwt =userRepository.save(JwtUser(outputDto.email,outputDto.firstName,outputDto.lastname,encoder.encode(outputDto.password),outputDto.email,true))
//        val customer = customerRepository.save(Customer(firstName = "xxx"))
//        jwt.user = customer
//        customer.jwtUser = jwt
//        jwt.authorities.add(authority)


//
//        jwt.user = MapperUtil.INSTANCE.mapUser(outputDto)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }

    @PutMapping("/customer/{id}")
    fun updateCustomer(@PathVariable id: Long, @RequestBody customer: Customer): ResponseEntity<Any> {
        customer.id = id
        return ResponseEntity.ok(customerService.save(customer))
    }

    @PostMapping("/customer/image/{id}")
    fun uploadFile(@RequestPart("file") file: MultipartFile, @PathVariable id: Long): ResponseEntity<Any> {

        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customerService.saveImgUrl(id,file)))


    }
}