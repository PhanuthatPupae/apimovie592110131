package com.moviesapi.demo.service

import com.moviesapi.demo.dao.CycleTimeDao
import com.moviesapi.demo.entity.CycleTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class CycleTimeServiceImpl:CycleTimeService {


    @Autowired
    lateinit var cycleTimeDao: CycleTimeDao
    override fun getAllCycleTime(): List<CycleTime> {
        return  cycleTimeDao.getAllCycleTime()
    }
    override fun getCycletimeByMovieName(name: String): List<CycleTime> {
        return cycleTimeDao.getCycleTimeByMovie(name)
    }
}