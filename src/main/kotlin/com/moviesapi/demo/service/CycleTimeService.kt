package com.moviesapi.demo.service

import com.moviesapi.demo.entity.CycleTime

interface CycleTimeService {
    fun getAllCycleTime():List<CycleTime>
    fun getCycletimeByMovieName(name:String):List<CycleTime>

}