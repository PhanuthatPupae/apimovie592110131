package com.moviesapi.demo.service

import com.moviesapi.demo.entity.Customer
import org.springframework.web.multipart.MultipartFile


interface CustomerService {
    fun getCustomers() :List<Customer>
    fun save(mapCustomer: Customer): Customer
   fun saveImgUrl(id:Long, file: MultipartFile): Customer


}