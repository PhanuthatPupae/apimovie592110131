package com.moviesapi.demo.service

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.dao.SoundDao
import com.moviesapi.demo.entity.Dto.SoundDto
import com.moviesapi.demo.entity.Sound
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional


@Service
class SoundServiceImpl : SoundService {
    @Transactional
    override fun save(sound: Sound): Sound {

        return soundDao.save(sound)
    }


    @Autowired
    lateinit var soundDao: SoundDao

    override fun getAllSound(): List<Sound> {
        return soundDao.getAllSound()
    }
}