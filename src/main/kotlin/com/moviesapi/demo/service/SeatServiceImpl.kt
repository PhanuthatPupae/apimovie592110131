package com.moviesapi.demo.service

import com.moviesapi.demo.dao.SeatDao
import com.moviesapi.demo.entity.Seat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SeatServiceImpl:SeatService {
    @Autowired
    lateinit var seatDao: SeatDao
    override fun getSeatType(): List<Seat> {
        return seatDao.getSeat()
    }
}