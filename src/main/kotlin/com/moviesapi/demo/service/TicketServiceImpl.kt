package com.moviesapi.demo.service

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.dao.CustomerDao
import com.moviesapi.demo.dao.CycleTimeDao
import com.moviesapi.demo.dao.TicketDao
import com.moviesapi.demo.entity.Dto.TicketDto
import com.moviesapi.demo.entity.Ticket
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional


@Service
class TicketServiceImpl : TicketService {



    @Autowired
    lateinit var ticketDao: TicketDao
    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var cycleTimeDao: CycleTimeDao

    override fun remove(id: Long): Ticket? {
        val ticket = ticketDao.findById(id)
        ticket?.seatStatus = false
        return ticket
    }
    override fun saveTicketByList( mapTicket: MutableIterable<Ticket>): MutableIterable<Ticket> {

        val ticket = ticketDao.saveList(mapTicket)


        return ticketDao.saveList(ticket)
    }


    @Transactional
    override fun saveAll(mapTicket: Ticket): Ticket {
        val cycle = mapTicket.cycleTime?.let { cycleTimeDao.save(it) }

        val mapTicket = ticketDao.save(mapTicket)

        return ticketDao.save(mapTicket)
    }

    @Transactional
    override fun saveTicketByCycleTime(id: Long, mapTicket: Ticket): Ticket {
        val cycle = cycleTimeDao.findById(id)
        val ticket = ticketDao.save(mapTicket)
        ticket.cycleTime = cycle
        return ticket
    }


    @Transactional
    override fun save(id: Long, mapTicket: Ticket): Ticket {
        val customer = customerDao.findById(id)
        val ticket = ticketDao.save(mapTicket)
        ticket.customer = customer

        return ticket


    }
    @Transactional
    override fun updateBycustomer(idCustomer: Long, mapTicket: Ticket): Ticket {
       val customer =customerDao.findById(idCustomer)
        val ticket = ticketDao.save(mapTicket)
        ticket.customer = customer
        return ticket
    }

    override fun getTicketByCustomer(email: String, firstName: String): List<Ticket> {
        return ticketDao.getTicketByCustomer(email, firstName)
    }

    override fun getTicketByMovie(name: String): List<Ticket> {
        return ticketDao.getTicketByMovie(name)
    }


    override fun getAllTicket(): List<Ticket> {
        return ticketDao.getAllTicket()
    }
}