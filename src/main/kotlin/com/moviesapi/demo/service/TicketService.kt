package com.moviesapi.demo.service

import com.amazonaws.services.dynamodbv2.document.Page
import com.moviesapi.demo.entity.Dto.TicketDto
import com.moviesapi.demo.entity.Ticket


interface TicketService {
    fun getAllTicket():List<Ticket>
    fun getTicketByMovie(name:String):List<Ticket>
    fun getTicketByCustomer(email:String,firstName:String):List<Ticket>
    fun saveTicketByList( mapTicket: MutableIterable<Ticket>): MutableIterable<Ticket>
    fun saveTicketByCycleTime(id: Long, mapTicket: Ticket): Ticket
    fun save(id: Long, mapTicket: Ticket): Ticket
    fun saveAll(Ticket: Ticket):Ticket
    fun remove(id: Long): Ticket?
    fun updateBycustomer(idCustomer :Long,mapTicket: Ticket): Ticket

}