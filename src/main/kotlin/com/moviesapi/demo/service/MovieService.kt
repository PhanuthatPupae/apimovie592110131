package com.moviesapi.demo.service

import com.moviesapi.demo.entity.Movie

interface MovieService {
    fun getMovies ():List<Movie>
}