package com.moviesapi.demo.service

import com.moviesapi.demo.dao.MovieDao
import com.moviesapi.demo.entity.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MovieServiceImpl:MovieService {
    @Autowired
    lateinit var movieDao: MovieDao
    override fun getMovies(): List<Movie> {
        return movieDao.getMovies()
    }
}