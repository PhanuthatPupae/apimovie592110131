package com.moviesapi.demo.service

import com.moviesapi.demo.entity.Dto.SoundDto
import com.moviesapi.demo.entity.Sound

interface SoundService {
    fun getAllSound ():List<Sound>
    fun save(sound:Sound):Sound
}