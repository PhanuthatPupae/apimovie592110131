package com.moviesapi.demo.service


import com.moviesapi.demo.dao.SubtitleDao

import com.moviesapi.demo.entity.Subtitle
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SubtitleServiceImpl : SubtitleService {
    override fun save(sub: Subtitle): Subtitle {
        val subtitle = subtitleDao.save(sub)
        return subtitle
    }

    @Autowired
    lateinit var subtitleDao: SubtitleDao

    override fun getAllSubtitle(): List<Subtitle> {
        return subtitleDao.getAllSubtitle()
    }
}