package com.moviesapi.demo.Util

import com.moviesapi.demo.entity.*
import com.moviesapi.demo.entity.Dto.*
import com.moviesapi.demo.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }


    fun mapCinemaDto(cinema: Cinema): CinemaDto
    fun mapCinemaDto(cinema: List<Cinema>): List<CinemaDto>


    fun mapSubtitleDto(subtile: Subtitle): SubtitleDto
    fun mapSubTitleDto(subtile: List<Subtitle>): List<SubtitleDto>


    @InheritInverseConfiguration
    fun mapSound(sound: SoundDto): Sound

    fun mapSoundDto(sound: Sound): SoundDto
    fun mapSoundDto(sound: List<Sound>): List<SoundDto>

    fun mapMovieDto(movie: Movie): MovieDto
    fun mapMovieDto(movie: List<Movie>): List<MovieDto>


    fun mapCycleTimeDto(cycleTime: CycleTime): CycleTimeDto

    fun mapCyCleTimeDto(cycleTime: List<CycleTime>): List<CycleTimeDto>

    fun mapSeatDto(seat: Seat): SeatDto
    fun mapSeatDto(seat: List<Seat>): List<SeatDto>

    @InheritInverseConfiguration
    fun mapTicket(ticketDto: TicketDto): Ticket

    @InheritInverseConfiguration
    fun mapTicketList(ticketDto: MutableIterable<TicketDto>): MutableIterable<Ticket>

    @InheritInverseConfiguration
    fun mapCustomer(customerDto: CustomerDto): Customer

    fun mapCustomerDto(customer: Customer): CustomerDto
    fun mapCustomerDto(customer: List<Customer>): List<CustomerDto>

    fun mapTicketDto(ticket: Ticket): TicketDto
    fun mapTicketDto(ticket: List<Ticket>): List<TicketDto>


    fun mapMovieDtoForCycleTime(movie: Movie): MovieDtoForCycleTime
    fun mapMovieDtoForCycleTime(movie: List<Movie>): List<MovieDtoForCycleTime>
    fun mapUser(currentUser: Customer): UserDto

    fun mapAuthority(authority: AuthorityDto): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>
    fun mapTicketLists(output: MutableIterable<Ticket>): MutableIterable<Ticket>
}